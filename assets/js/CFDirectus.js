if (typeof CloudFaces == 'undefined') {
    CloudFaces = { Directus: { Config: {} }}
} else {
    CloudFaces.Directus = {Config: {}};
}

var url             = 'https://directus.cloudfaces.com';
var projectId       = 9876;
var DirectusClient  = new DirectusSDK({
    url: url,
    project: projectId
});
var currentPage = location.pathname.split('/').pop();

$.extend(CloudFaces.Directus, {
    directusLogin: function(params) {
        //console.log('Params for directusLogin:', params);
        return new Promise(function (resolve, reject) {
            DirectusClient.login({
                email: params.email,
                password: params.password,
            }).then(function (token) {
                DirectusClient.getMe().then(function (userData) {
                    var currentUserData = CloudFaces.Directus.getDirectusUser() ? Object.assign(CloudFaces.Directus.getDirectusUser(), params) : params;
                    //console.log('currentuser data:' + JSON.parse(currentUserData));
                    var newUserData = Object.assign(currentUserData, userData.data, {token: token});
                    CloudFaces.Directus.setDirectusUser(newUserData);
                    resolve(newUserData);
                }).catch(function (error) {
                    reject(error);
                });
            }).catch(function (error) {
                // re-assign token
                DirectusClient.token = CloudFaces.Directus.getCurrentDirectustoken();
                reject(error);
            });
        });
    },

    /**
     * Store user data to local storage
     * @param {*} data
     */
    setDirectusUser: function(data) {
        // delete data['password'];
        localStorage.setItem('cf_directus_'+projectId+'_user', JSON.stringify(data));
    },

    /**
     * Get current user data
     */
    getDirectusUser: function() {
        return localStorage.getItem('cf_directus_'+projectId+'_user') ?
            JSON.parse(localStorage.getItem('cf_directus_'+projectId+'_user')) : '';
    },

    /**
     * Get current user directus token
     */
    getCurrentDirectustoken() {
        var currentUserData = CloudFaces.Directus.getDirectusUser() ? CloudFaces.Directus.getDirectusUser() : "";
        var token = currentUserData ? currentUserData.token.token : "";
        return token;
    },

    /**
     * Update user diretus token
     *
     * @param {String} token
     */
    updateDirectusToken: function(token) {
        var userData = CloudFaces.Directus.getDirectusUser();
        if (token && userData) {
            var newTokenData = Object.assign({}, userData.token, {token: token});
            var newUserData = Object.assign(userData, {token: newTokenData});
            DirectusClient.token = token;
            CloudFaces.Directus.setDirectusUser(newUserData);
        }
    },

    /**
     * Check if user is logged in
     */
    checkLogin: function(){
        var userData = CloudFaces.Directus.getDirectusUser();
        if (!userData) {
            CFMenuNavigation.navigate(CloudFaces.SandBoxx.Config.pages.login, '');
            return false;
        }
        return true;
    },

    /**
     * Process the registration
     *
     * @param {object} params
     */
    directusRegisterUser: function(params) {
        // console.log('register function params:' + JSON.stringify(params));
        return new Promise(function (resolve, reject) {
            DirectusClient.getMe().then(function (user) {
                var user = user.data;
                // console.log('get me user:' + JSON.stringify(user));
                var newUserData = {
                    email: params.email,
                    password: params.password,
                    first_name: params.email, // Since we don't have name field, set email for now
                    registered: true, // Set user as registered user
                    company:params.company_id // Attach the company to user
                };
                DirectusClient.updateItem('directus_users', user.id, newUserData).then(function (response) {
                    var currentUserData = Object.assign(CloudFaces.Directus.getDirectusUser(), newUserData);
                    CloudFaces.Directus.setDirectusUser(currentUserData);
                    resolve(response);
                }).catch(function (error) {
                    reject(error);
                });
            });
        });
    },

    /**
     * Log out the current user
     */
    directusLogoutUser: function() {
        localStorage.removeItem('cf_directus_'+projectId+'_user');
        DirectusClient.logout();
        CloudFaces.Directus.redirectToLogin();
    },

    /**
     * Store the feedback to Directus
     *
     * @param {String} email
     * @param {String} text
     */
    sendFeedback: function(email, text) {
        return new Promise(function(resolve, reject) {
            var feedbackData = {
                email: email,
                text: text,
            };
            DirectusClient.createItem('feedback', feedbackData).then(resolve);
        });
    },

    /**
     * Fetch the list of FAQs
     */
    getFaqs: function() {
        return new Promise(function(resolve, reject) {
            DirectusClient.getItems('faq').then(resolve);
        });
    },

    /**
     * Get details of single faq
     * @param {int} faq_id
     */
    getFaq: function(faq_id) {
        return new Promise(function(resolve, reject) {
            DirectusClient.getItem('faq', faq_id).then(resolve);
        });
    },

    /**
     * Request the Reset the password token for user
     *
     * @param {String} email
     */
    requestResetPassword: function(email) {
        return new Promise(function(resolve, reject) {
            DirectusClient.requestPasswordReset(email).then(resolve);
        });
    },

    /**
     * Reset the password for user
     * Note: DirectusClient.resetPassword is not present in sdk-js at this moment
     *
     * @param {String} token
     * @param {String} password
     */
    resetPassword: function(token, password, callback) {
        var data = {
            token: token,
            password: password
        };
        var directus_token = CloudFaces.Directus.getDirectusUser().token.token;
        var ajaxParams = {
            url: url + '/' + projectId + '/auth/password/reset',
            type: 'post',
            data: data,
            cache:false,
            headers: {"Authorization": 'Bearer '+ directus_token},
            success: function(data) {
                //console.log('data from success ' + JSON.stringify(data));
                callback(data);
            },
            error: function(error) {
                //console.log('data from error ' + JSON.stringify(error));
                callback(JSON.parse(error.responseText));
            }
        };

        $.ajax(ajaxParams);
    },

    removeItem: function(collection, itemId, callback) {
        var directus_token = CloudFaces.Directus.getDirectusUser().token.token;

        var ajaxParams = {
            url: url + '/' + projectId + '/items/'+ collection +'/'+ itemId,
            type: 'delete',
            cache:false,
            headers: {"Authorization": 'Bearer '+ directus_token},
            success: function(data) {
                //console.log('data from success ' + JSON.stringify(data));
                callback(data);
            },
            error: function(error) {
                //console.log('data from error ' + JSON.stringify(error));
                callback(error);
            }
        };

        $.ajax(ajaxParams);
    },

    redirectToLogin: function () {
        if (currentPage != "login.html") {
            return location.href = 'login.html';
        }
        cfInit();
    }
});

function cfInit() {
    $(document).ready(function () {
        $(document).trigger('cf-initialized');
    });

}


// var deviceAin = "ain_"+(new Date()).getTime();
// var deviceCode = "code_"+(new Date()).getTime();
// if (deviceAin != '0' && deviceCode != '0') {
    var appUser = CloudFaces.Directus.getDirectusUser();
    var currentDateTimestamp = (new Date()).getTime();
    if (typeof appUser != "undefined" && appUser) {
        var userTokenValue = appUser.token.token;
        if (userTokenValue) {
            DirectusClient.refresh(userTokenValue).then(function (data) {
                var newToken = data.data.token;
                DirectusClient.token = newToken;
                CloudFaces.Directus.updateDirectusToken(newToken);
                if (currentPage == "login.html") {
                    location.href = "index.html";
                }
                cfInit();
            }).catch(function (refreshError) {
                console.log('refreshError', refreshError);
                CloudFaces.Directus.redirectToLogin();
            })
        } else {
            CloudFaces.Directus.redirectToLogin();
        }
    } else {
        CloudFaces.Directus.redirectToLogin();
        // var dateTimestamp = currentDateTimestamp.toString();
        // var userEmail = "a_" + deviceAin + "_" + dateTimestamp + "@cloudfaces.com";
        // var userPassword = "Anonymous_" + deviceCode;
        // var userData = {
        //     status: "active",
        //     role: 3,
        //     first_name: "Anonymous_" + deviceAin + "_" + dateTimestamp,
        //     email: userEmail,
        //     password: userPassword,
        //     company: "Anonymous Company",
        //     title: "Anonymous User_" + deviceAin,
        //     device_ain: deviceAin,
        //     device_code: deviceCode
        // };
        // DirectusClient.createItem("directus_users", userData).then(function (data) {
        //     CloudFaces.Directus.directusLogin(userData).then(function (response) {
        //         cfInit();
        //     });
        // });
    }
// }